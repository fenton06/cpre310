public class Main {

    public static void main(String[] args) {

        long startTime = System.nanoTime();

        double n = iter(1000);

        //divide by 1000000 to get milliseconds.
        long duration = System.nanoTime() - startTime;

        System.out.println(n);
        System.out.println("Execution time (ns): " + duration);

    }

    private static double rec(int n) {

        double result;

        if(n == 0) { result = 1.0;}
        else if(n == 1) { result = 2.0;}
        else if(n == 2) { result = 3.0;}
        else { result = rec(n - 1) + rec(n - 2) + rec(n - 3);}

        return result;
    }

    private static double iter(int n) {

        double result = 0.0;

        if(n == 0) { result = 1.0; }
        else if(n == 1) { result = 2.0; }
        else if(n == 2) { result = 3.0;}
        else {
            double x = 1.0;
            double y = 2.0;
            double z = 3.0;

            for(int i = 0; i < n - 2; i++) {
                result = x + y + z;
                x = y;
                y = z;
                z = result;
            }
        }
        return result;
    }

}
